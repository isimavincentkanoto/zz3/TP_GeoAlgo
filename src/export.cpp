#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/boost/graph/graph_traits_Polyhedron_3.h>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <CGAL/Simple_cartesian.h>
#include <CGAL/Polyhedron_3.h>

#include <iostream>
#include <fstream>
#include <map>
#include <algorithm>
#include <numeric>
#include <vector>

typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;
typedef CGAL::Polyhedron_3<Kernel> Polyhedron;

typedef Polyhedron::Facet_const_iterator Facet_iterator;
typedef Polyhedron::Vertex_const_iterator Vertex_iterator;
typedef Polyhedron::Halfedge_const_iterator Halfedge_iterator;
typedef Polyhedron::Halfedge_around_facet_const_circulator Halfedge_facet_circulator;
typedef Kernel::Point_3 Point_3;

typedef std::map<Polyhedron::Facet_const_handle, double> Facet_double_map;
typedef std::map<Polyhedron::Facet_const_handle, int> Facet_int_map;

std::vector<Color_t> colorClasses;

/// @brief Generate in a .off file a colored mesh according to a value map (green to red shades)
/// @param mesh the input mesh
/// @param facetMap map of values between 0 and 1 (see "normalize()") for each facet of mesh
/// @param filePath path to the colored .off file to be generated
void writeOFFfromValueMap(const Polyhedron& mesh, const Facet_int_map& facetMap, std::string filePath){
	std::ofstream in_myfile;
	in_myfile.open(filePath);

	CGAL::set_ascii_mode(in_myfile);

	in_myfile << "COFF" << std::endl // "COFF" makes the file support color informations
			  << mesh.size_of_vertices() << ' ' 
			  << mesh.size_of_facets() << " 0" << std::endl; 
			  // nb of vertices, faces and edges (the latter is optional, thus 0)

	std::copy(mesh.points_begin(), mesh.points_end(),
			  std::ostream_iterator<Kernel::Point_3>(in_myfile, "\n"));

	//Facet_int_map result = multipleThreshold(mesh, facetMap);

	Color_t c;
	for (auto i = facetMap.begin(); i != facetMap.end(); ++i)
	{
		Halfedge_facet_circulator j = i->first->facet_begin();

		CGAL_assertion(CGAL::circulator_size(j) >= 3);

		in_myfile << CGAL::circulator_size(j) << ' ';
		do
		{
			in_myfile << ' ' << std::distance(mesh.vertices_begin(), j->vertex());

		} while (++j != i->first->facet_begin());

		in_myfile << std::setprecision(5) << std::fixed; //set the format of floats to X.XXXXX
		
		int faceClass = i->second;
		
		c.red = colorClasses[faceClass].red; // low values will be closer to red
		c.green = colorClasses[faceClass].green; // high values will be closer to green
		c.blue = colorClasses[faceClass].blue;

		in_myfile << " " << c.red << " " << c.green << " " << c.blue;

		in_myfile << std::endl;
	}

	in_myfile.close();

	std::cout << "Le résultat a été exporté dans " << filePath << " !" << std::endl;
}

/*
// Write an OFF file representing an AABB
void writeAABB(std::ofstream& out, const AABB& bb, const CGAL::Color& color)
{
    out << "OFF\n";
    out << "8 12 0\n";
    out << bb.minCorner.x << " " << bb.minCorner.y << " " << bb.minCorner.z << "\n";
    out << bb.maxCorner.x << " " << bb.minCorner.y << " " << bb.minCorner.z << "\n";
    out << bb.maxCorner.x << " " << bb.maxCorner.y << " " << bb.minCorner.z << "\n";
    out << bb.minCorner.x << " " << bb.maxCorner.y << " " << bb.minCorner.z << "\n";
    out << bb.minCorner.x << " " << bb.minCorner.y << " " << bb.maxCorner.z << "\n";
    out << bb.maxCorner.x << " " << bb.minCorner.y << " " << bb.maxCorner.z << "\n";
    out << bb.maxCorner.x << " " << bb.maxCorner.y << " " << bb.maxCorner.z << "\n";
    out << bb.minCorner.x << " " << bb.maxCorner.y << " " << bb.maxCorner.z << "\n";
    out << "4 0 1 2 3 " << CGAL::Color(color) << "\n";
    out << "4 7 6 5 4 " << CGAL::Color(color) << "\n";
    out << "4 0 4 5 1 " << CGAL::Color(color) << "\n";
    out << "4 1 5 6 2 " << CGAL::Color(color) << "\n";
    out << "4 2 6 7 3 " << CGAL::Color(color) << "\n";
    out << "4 3 7 4 0 " << CGAL::Color(color) << "\n";
}


// Write an OFF file representing the octree
void writeOctree(std::ofstream& out, const OctreeNode& octree)
{
    // Write the bounding box of the root node
    CGAL::Color color(255, 0, 0);
    writeAABB(out, octree.cube, color);
}
*/