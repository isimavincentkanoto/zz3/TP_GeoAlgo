void WriteOFFsimplified(const Polyhedron &mesh){
	std::vector<int> temp;
	Point3 prec;

	int index_saver, index2_saver;
	double minDistance = 1000;
	//for(auto it=mesh.vertex_handles().begin(); it!= mesh.vertex_handles().end(); ++it){
	for(int i=0; i<listPoints.size(); ++i){
		for(int j=0; j<listPoints.size(); ++j){
			//for(auto vert : listPoints[i].vertexlist){
			//if(*it == vert){
				if(std::find(temp.begin(), temp.end(), i) == temp.end()){
					if(temp.size() > 0){
						double curDistance = Euclidian3Ddistance(listPoints[i].pt, prec);
						if(curDistance != 0){
							if(minDistance > curDistance){
								minDistance = curDistance;
								index2_saver = index_saver;
								index_saver = i;
							}
						}
					}
					else{
						temp.push_back(i);
						prec = listPoints[i].pt;
					}
				}
				/*
				if(temp.size()>2){
					faces.push_back(temp);
					temp.clear();
				}*/
				//break;
			//}
		//}
		temp.push_back(index2_saver);
		temp.push_back(index_saver);
		faces.push_back(temp);
		temp.clear();
		}
	}
	//}

	std::ofstream out("octree_meshresSimplified.off");
	out << "OFF" << std::endl;
	out << listPoints.size() << " " << 3*faces.size() << " " << faces.size() << std::endl;
	for(auto point : listPoints){
		out << point.pt.x() << " " << point.pt.y() << " " << point.pt.z() << "\n";
	}
	for(auto face : faces){
		out << face.size() << " ";
		for(auto fi : face){
			out << fi << " ";
		}
		out << std::endl;
	}
}





void WriteOFFsimplified(const Polyhedron &mesh){
	std::vector<int> temp;
	Point3 prec;

	for(auto it=mesh.vertex_handles().begin(); it!= mesh.vertex_handles().end(); ++it){
		for(int i=0; i<listPoints.size(); ++i){
			for(auto vert : listPoints[i].vertexlist){
				if(*it == vert){
					if(std::find(temp.begin(), temp.end(), i) == temp.end()){
						if(temp.size() > 0){
							if(isCloseEnough(vert->point(), prec)){
								temp.push_back(i);
								prec = vert->point();
							}
						}
						else{
							temp.push_back(i);
							prec = vert->point();
						}
					}
					if(temp.size()>2){
						faces.push_back(temp);
						temp.clear();
					}
					break;
				}
			}
			
		}
	}

	std::ofstream out("octree_meshresSimplified.off");
	out << "OFF" << std::endl;
	out << listPoints.size() << " " << 3*faces.size() << " " << faces.size() << std::endl;
	for(auto point : listPoints){
		out << point.pt.x() << " " << point.pt.y() << " " << point.pt.z() << "\n";
	}
	for(auto face : faces){
		out << face.size() << " ";
		for(auto fi : face){
			out << fi << " ";
		}
		out << std::endl;
	}
}




void WriteOFFsimplified(const Polyhedron &mesh){
	std::vector<int> temp;
	Point3 prec;

	int index_saver, index2_saver;
	double minDistance = 1000;
	for(int i=0; i<listPoints.size(); ++i){
		for(int j=0; j<listPoints.size(); ++j){
			if(std::find(temp.begin(), temp.end(), i) == temp.end()){
				if(temp.size() > 0){
					double curDistance = Euclidian3Ddistance(listPoints[i].pt, prec);
					if(curDistance != 0){
						if(minDistance > curDistance){
							minDistance = curDistance;
							index2_saver = index_saver;
							index_saver = i;
						}
					}
				}
				else{
					temp.push_back(i);
					prec = listPoints[i].pt;
				}
			}
		//temp.push_back(index2_saver);
		//temp.push_back(index_saver);
		faces.push_back(temp);
		temp.clear();
		}
	}

	std::ofstream out("octree_meshresSimplified.off");
	out << "OFF" << std::endl;
	out << listPoints.size() << " " << 3*faces.size() << " " << faces.size() << std::endl;
	for(auto point : listPoints){
		out << point.pt.x() << " " << point.pt.y() << " " << point.pt.z() << "\n";
	}
	for(auto face : faces){
		out << face.size() << " ";
		for(auto fi : face){
			out << fi << " ";
		}
		out << std::endl;
	}
}